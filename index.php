<?php
$message = isset($_GET['message']) ? $_GET['message'] : '';
$count = isset($_GET['count']) ? $_GET['count'] : 1;
?>

<form style="border:1px solid gray; padding:.5rem; border-radius: 10px" action="index.php" method="GET">
    <label style="display:block; margin-bottom: 1rem">
        <p style="margin-bottom: .75rem">Quel message ?</p>
        <input style="display:block" name="message" value="<?php echo ($message) ?>">
    </label>
    <label style="display:block; margin-bottom: 1rem">
        <p style="margin-bottom: .75rem">Combien de messages ?</p>
        <input style="display:block" type="range" name="count" min="1" max="7" value="<?php echo ($count) ?>" />
    </label>

    <button>Envoyer</button>
</form>

<?php
if ($message !== '' && $count > 0) {
    for ($i = 1; $i <= $count; $i++) {
        echo '<p style="font-size:' . $i . 'rem">' . $message . '</p>';
    }

    for ($i = $count; $i > 0; $i--) {
        echo '<p style="font-size:' . $i . 'rem">' . $message . '</p>';
    }
}
?>