<p>Bataille navale</p>

<?php
// Start or pick existing session
session_start();

// Set $p with request parameter `p=` or init it with empty value
// This var will be used to reset game and define message
$p = isset($_GET['p']) ? $_GET['p'] : '';

// Set $size with request parameter `size=` or init it as number 3
// This var will be used to define table size
$size = isset($_GET['size']) ? $_GET['size'] : 3;

// If `nbalea` session var doesn't exists or if $p var equal `raz` 
// create and store new random number in session
if (!isset($_SESSION['nbalea']) || $p === 'raz') {
    $_SESSION['nbalea'] = (string) rand(1, 3) * 10 + rand(1, 3);
}

// Create shortcut to `nbalea` session var
$nbalea = $_SESSION['nbalea'];

// Check if strings $p and $nbalea are strictly (===) the same
// If (?) they are the same, set message as `TROUVE !!!!` else (:) set message as `PLOUF`
$message = (string) $p === (string) $nbalea ? 'TROUVE !!!!' : 'PLOUF';

// If $p var strictly equal `raz`, set $message var to empty string
if ($p === 'raz') {
    $message = '';
}

// Check if $p var is strictly different than empty string ('') and
// strictly different than `raz`
if ($p !== '' && $p !== 'raz') {
    echo ('<p>' . $message . '</p>');
}
?>

<table>
    <tbody>
        <?php
        for ($line = 1; $line <= $size; $line++) {
            echo "<tr>";
            for ($cell = 1; $cell <= $size; $cell++) {
                $number = $line . $cell;

                echo "<td><a href='/batnav.php?p=$number&size=$size'>$number</a></td>";
            }
            echo "</tr>";
        }
        ?>
    </tbody>
</table>

<?php
// Create reset button using the `raz` value for $p var
// To avoid size redefinition, use $size var
echo "<a href='/batnav.php?p=raz&size=$size'>RAZ</a>";
?>